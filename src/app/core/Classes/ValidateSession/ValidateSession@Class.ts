import { UserModel } from '../../Models/User/User@Model';

export class ValidateSessionClass {
    validateSession(data: UserModel) {
        if (data.id > 0) {
            return '/container';
        }

        return '/login';
    }
}