import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';
import { SharedModule } from '~/app/shared/shared.module';
import { DetailsComponent } from './components/details/details.component';
import { PoliciesComponent } from './components/policies/policies.component';
import { ViewComponent } from './components/view/view.component';
import { PoliciesRoutingModule } from './policies-routing.module';



@NgModule({
    imports: [
        NativeScriptCommonModule,
        PoliciesRoutingModule,
        SharedModule
    ],
    exports: [],
    declarations: [
        PoliciesComponent,
        ViewComponent,
        DetailsComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class PoliciesModule { }
